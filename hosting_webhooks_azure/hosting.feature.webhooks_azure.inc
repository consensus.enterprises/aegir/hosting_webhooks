<?php
/**
 * @file
 * The hosting feature definition for hosting_webhooks_azure.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_webhooks_azure_hosting_feature() {
  $features['webhooks_azure'] = [
    'title' => t('Webhook Azure scale set integration'),
    'description' => t('Provides webhook plugins for Azure scale set operations.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_webhooks_azure',
    'group' => 'experimental',
  ];

  return $features;
}
