<?php
/**
 * @file
 * Aegir Scale Set Azure Trait.
 */

namespace HostingWebhooks;

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks_azure') . '/src/RestAzureTrait.php';

use \HostingWebhooks\RestAzureTrait;

/**
 * Trait that provides functionality for Azure scale set operations.
 */
trait ScaleSetAzureTrait {

  use RestAzureTrait;

  // Pseudo-constants, since PHP doesn't allow constants in traits.
  static $AZURE_VMSS_API_VERSION = '2020-12-01';
  static $AZURE_VM_LIST_RESOURCE_PATH = 'subscriptions/%subscriptionId/resourceGroups/%resourceGroupName/providers/Microsoft.Compute/virtualMachineScaleSets/%virtualMachineScaleSetName/virtualMachines';
  static $AZURE_PUBLIC_IP_API_VERSION = '2018-10-01';
  static $AZURE_PUBLIC_IP_RESOURCE_PATH = 'subscriptions/%subscriptionId/resourceGroups/%resourceGroupName/providers/Microsoft.Compute/virtualMachineScaleSets/%virtualMachineScaleSetName/virtualMachines/%virtualmachineIndex/networkInterfaces/%networkInterfaceName/ipconfigurations/%ipConfigurationName/publicipaddresses';
  static $AZURE_NETWORK_IF_API_VERSION = '2017-03-30';
  static $AZURE_NETWORK_IF_RESOURCE_PATH = 'subscriptions/%subscriptionId/resourceGroups/%resourceGroupName/providers/Microsoft.Compute/virtualMachineScaleSets/%virtualMachineScaleSetName/virtualMachines/%virtualmachineIndex/networkInterfaces/%networkInterfaceName/ipconfigurations/%ipConfigurationName';

  /**
   * Wait until the scale set operation is complete.
   *
   * Azure scale set webhooks fire when the scale operation is triggered. If we
   * act on API query VM data right away, we'll get the pre-scale data.
   *
   * Instead, we wait for the number of scale set VMs to match the new VM count
   * specified in the webhook
   */
  protected function waitForScaleSetOperationToComplete() {
    if ($this->countScaleSetVms() == $this->getNewScaleSetCapacity()) return;
    $this->logNotice('Waiting for Azure scale set @scale_op operation to complete.');
    $timeout = variable_get('hosting_webhooks_scaleset_timeout', 300);
    $timer = 0;
    $increment = 1;
    while ($this->countScaleSetVms() != $this->getNewScaleSetCapacity()) {
      if ($timer >= $timeout) {
        return $this->logFailure('Number of scale set VMs does not match expected capacity.', '408 Request Timeout', 'Timeout waiting for scale set operation to complete.');
      }
      sleep($increment);
      $timer += $increment;
    }
    $this->logNotice('Azure scale set @scale_op operation completed. Proceeding.');
  }

  /**
   * Return the current number of scale set VMs.
   */
  protected function countScaleSetVms() {
    return count($this->getScaleSetVmData());
  }

  /**
   * Return the new number of scale set VMs.
   */
  protected function getNewScaleSetCapacity() {
    return $this->getPayload()->context->newCapacity;
  }

  /**
   * Return a list of scale set VM IP addresses.
   */
  protected function getScaleSetVmIps() {
    $ips = [];
    foreach ($this->getScaleSetVmData() as $vm) {
      $ips[] = $this->getScaleSetVmIp($vm);
    }
    return $ips;
  }

  /**
   * Get raw scale set VM data from the Azure REST API.
   */
  protected function getScaleSetVmData() {
    $vm_data = $this->getRestData(self::$AZURE_VM_LIST_RESOURCE_PATH, $this->getVmssRestConfig());
    return json_decode($vm_data)->value;
  }

  /**
   * Get the IP of a scale set VM from the Azure REST API.
   */
  protected function getScaleSetVmIp($vm) {
    $ip_data = $this->getScaleSetVmIpData($vm);

    switch (variable_get('hosting_azure_use_public_private_ips', 'public')) {
      case 'private':
        return $ip_data->properties->privateIPAddress;
      case 'public':
      default:
        return $ip_data->properties->ipAddress;
    }
  }

  /**
   * Get raw public IP config data from the Azure REST API.
   */
  protected function getScaleSetVmIpData($vm) {
    $vm_index = $vm->instanceId;
    $net_if_conf = $vm->properties->networkProfileConfiguration->networkInterfaceConfigurations[0];
    $net_if_name = $net_if_conf->name;
    $ip_conf_name = $net_if_conf->properties->ipConfigurations[0]->name;

    switch (variable_get('hosting_azure_use_public_private_ips', 'public')) {
      case 'private':
        $rest_config = $this->getNetworkInterfaceRestConfig($vm_index, $net_if_name, $ip_conf_name);
        $ip_data = $this->getRestData(self::$AZURE_NETWORK_IF_RESOURCE_PATH, $rest_config);
        return json_decode($ip_data);
      case 'public':
      default:
        $rest_config = $this->getPublicIpRestConfig($vm_index, $net_if_name, $ip_conf_name);
        $ip_data = $this->getRestData(self::$AZURE_PUBLIC_IP_RESOURCE_PATH, $rest_config);
        return json_decode($ip_data)->value[0];
    }
  }

  /**
   * Return a REST config array for querying the Azure Virtual Machine Scale Set API.
   */
  protected function getVmssRestConfig() {
    $config = $this->getBaseRestConfig();
    $config['query']['api-version'] = self::$AZURE_VMSS_API_VERSION;
    $config['parameters']['%virtualMachineScaleSetName'] = $this->getVirtualMachineScaleSetName();
    return $config;
  }

  /**
   * Return a REST config array for querying the Azure Public IP API.
   */
  protected function getPublicIpRestConfig(string $vm_index, string $net_if_name, string $ip_conf_name) {
    $config = $this->getNetworkInterfaceRestConfig($vm_index, $net_if_name, $ip_conf_name);
    $config['query']['api-version'] = self::$AZURE_PUBLIC_IP_API_VERSION;
    return $config;
  }

  /**
   * Return a REST config array for querying the Azure Network Interface API.
   */
  protected function getNetworkInterfaceRestConfig(string $vm_index, string $net_if_name, string $ip_conf_name) {
    $config = $this->getVmssRestConfig();
    $config['query']['api-version'] = self::$AZURE_NETWORK_IF_API_VERSION;
    $config['parameters']['%virtualmachineIndex'] = $vm_index;
    $config['parameters']['%networkInterfaceName'] = $net_if_name;
    $config['parameters']['%ipConfigurationName'] = $ip_conf_name;
    return $config;
  }

  /**
   * Return the name of the Azure scale set from the webhook payload.
   */
  protected function getVirtualMachineScaleSetName() {
    $resource_id = $this->getPayload()->context->resourceId;
    $segments = explode('/', $resource_id);
    $vmss_name = end($segments);
    return check_plain($vmss_name);
  }

}
