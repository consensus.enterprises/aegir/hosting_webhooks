<?php
/**
 * @file
 * Aegir Rest Azure Trait.
 */

namespace HostingWebhooks;

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks_azure') . '/src/OAuth2AzureTrait.php';

use \HostingWebhooks\OAuth2AzureTrait;

/**
 * Trait that provides REST functionality for Azure scale set operations.
 */
trait RestAzureTrait {

  use OAuth2AzureTrait;

  // Pseudo-constants, since PHP doesn't allow constants in traits.
  static $AZURE_REST_ENDPOINT = 'https://management.azure.com';

  /**
   * Get raw data from the Azure REST API.
   */
  protected function getRestData($resource_path, $rest_config) {
    try {
      $response = restclient_get($resource_path, $rest_config);
    }
    catch (\Exception $e) {
      $this->logFailure($e->getMessage());
    }

    if ($response->code !== '200') {
      $this->logWarning('@plugin failed to retrieve data from the Azure REST API.');
      $error = '<pre>' . json_encode(json_decode($response->data), JSON_PRETTY_PRINT) . '</pre>';
      $this->logFailure($error);
    }

    return $response->data;
  }

  /**
   * Return a REST config array.
   */
  protected function getBaseRestConfig() {
    return [
      'endpoint' => self::$AZURE_REST_ENDPOINT,
      'query' => [],
      'parameters' => [
        '%subscriptionId' => $this->getSubscriptionId(),
        '%resourceGroupName' => $this->getResourceGroupName(),
        '%virtualMachineScaleSetName' => $this->getVirtualMachineScaleSetName(),
      ],
      'headers' => [
        'Authorization' => 'Bearer ' . $this->getAccessToken(),
      ],
      'error_handling' => TRUE,
    ];
  }

  /**
   * Return the Azure subscription ID from the webhook payload.
   */
  protected function getSubscriptionId() {
    return $this->getPayload()->context->subscriptionId;
  }

  /**
   * Return the name of the Azure resource group from the webhook payload.
   */
  protected function getResourceGroupName() {
    return $this->getPayload()->context->resourceGroupName;
  }

}
