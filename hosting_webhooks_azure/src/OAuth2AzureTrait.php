<?php
/**
 * @file
 * Aegir OAuth2 Azure Trait.
 */

namespace HostingWebhooks;

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks_azure') . '/src/AzureOAuth2Client.php';

use \HostingWebhooks\AzureOAuth2Client;

/**
 * Trait that provides OAuth2 functionality for Azure scale set operations.
 */
trait OAuth2AzureTrait {

  // Pseudo-constants, since PHP doesn't allow constants in traits.
  static $OAUTH2_SERVER_URL = 'https://login.microsoftonline.com/';
  static $OAUTH2_RESOURCE = 'https://management.azure.com';
  static $OAUTH2_AUTH_FLOW = 'client-credentials';

  /**
   * Fetch an access token, to authenticate further REST calls.
   */
  protected function getAccessToken() {
    $oauth2_config = $this->getOAuth2Config();
    $client_id = $this->getClientId();

    try {
      $oauth2_client = new AzureOAuth2Client($oauth2_config, $client_id);
      $redirect = FALSE;
      $access_token = $oauth2_client->getAccessToken($redirect);
    }
    catch (\Exception $e) {
      $this->logFailure($e->getMessage());
    }

    return $access_token;
  }

  /**
   * Return a valid OAuth2 token, to authenticate further REST calls.
   */
  protected function getOAuth2Config() {
    return [
      'token_endpoint' => self::$OAUTH2_SERVER_URL . $this->getTenantId() . '/oauth2/token',
      'auth_flow' => self::$OAUTH2_AUTH_FLOW,
      'client_id' => $this->getClientId(),
      'client_secret' => $this->getClientSecret(),
      'resource' => self::$OAUTH2_RESOURCE,
    ];
  }

  /**
   * Return the tenant ID.
   */
  protected function getTenantId() {
    return variable_get('hosting_azure_tenant_id', '');
  }

  /**
   * Return the client ID.
   */
  protected function getClientId() {
    return variable_get('hosting_azure_client_id', '');
  }

  /**
   * Return the client secret (password).
   */
  protected function getClientSecret() {
    return variable_get('hosting_azure_client_secret', '');
  }

}
