<?php
/**
 * @file
 * Webhook processor plugin class.
 */

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/BaseProcessor.php';
include_once drupal_get_path('module', 'hosting_webhooks_azure') . '/src/WebpackAzureTrait.php';

use \HostingWebhooks\BaseProcessor;
use \HostingWebhooks\WebpackAzureTrait;

/**
 * Webhook that logs incoming Azure scale set data.
 */
class LogAzureDataProcessor extends BaseProcessor {

  use WebpackAzureTrait;

}
