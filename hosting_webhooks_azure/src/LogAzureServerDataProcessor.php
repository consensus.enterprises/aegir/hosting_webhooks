<?php
/**
 * @file
 * Webhook processor plugin class.
 */

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/BaseProcessor.php';
include_once drupal_get_path('module', 'hosting_webhooks_azure') . '/src/WebpackAzureTrait.php';

use \HostingWebhooks\BaseProcessor;
use \HostingWebhooks\WebpackAzureTrait;

/**
 * Webhook that logs Azure scale set server data.
 */
class LogAzureServerDataProcessor extends BaseProcessor {

  use WebpackAzureTrait;

  /**
   * {@inheritdoc}
   */
  protected function doProcessActions() {
    $log_vars = $this->getLogVars();
    foreach ($this->getScaleSetVmIps() as $ip) {
      $log_vars['@ip'] = $ip;
      $this->logNotice('@plugin discovered scale set VM public IP address @ip', $log_vars);
    }
  }

}
