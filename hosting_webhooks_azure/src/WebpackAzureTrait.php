<?php
/**
 * @file
 * Aegir Webpack Azure Trait.
 */

namespace HostingWebhooks;

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks_azure') . '/src/ScaleSetAzureTrait.php';
include_once drupal_get_path('module', 'hosting_webhooks_azure') . '/src/BaseAzureTrait.php';

use \HostingWebhooks\ScaleSetAzureTrait;
use \HostingWebhooks\BaseAzureTrait;

/**
 * Trait that provides functionality for Azure webpack operations.
 */
trait WebpackAzureTrait {

  use ScaleSetAzureTrait;
  use BaseAzureTrait {
    getServerHostname as traitGetServerHostname;
  }

  /* The address of the VM currently being processed. */
  protected $ipAddress;

  /**
   * Remove any servers from the webpack cluster that are no longer in the Azure scale set.
   */
  protected function removeMissingServersFromWebpack() {
    $ips = $this->getScaleSetVmIps();
    foreach ($this->getMinionHostnames() as $server) {
      if (in_array($server, $ips)) continue;

      $this->ipAddress = $server;
      $this->logNotice('@plugin discovered webpack server (@hostname) missing from scale set.');

      $this->removeServerFromWebpack();
    }
  }

  /**
   * Add Azure scale set VMs to a webpack cluster.
   */
  protected function addScaleSetVmsToWebpack() {
    $log_vars = $this->getLogVars();
    foreach ($this->getScaleSetVmIps() as $ip) {
      $this->ipAddress = $log_vars['@ip'] = $ip;
      $this->logNotice('@plugin discovered scale set VM public IP address @ip', $log_vars);

      $this->AddServerToWebpack();
    }
  }

  /**
   * {@inheritdoc}
   *
   * By overriding this method, we can use the IP addresses from the loops in
   * removeMissingServersFromWebpack() and addScaleSetVmsToWebpack().
   */
  protected function getServerHostname() {
    return $this->ipAddress;
  }

  /**
   * {@inheritdoc}
   */
  protected function getServicesFromPayload() {
    $services = new \stdClass();
    $services->http = "apache";

    $properties = $this->getPayload()->properties;
    if (isset($properties->http_service)) {
      $services->http = check_plain($properties->http_service);
    }

    return $services;
  }

  /**
   * {@inheritdoc}
   */
  protected function getClusterType() {
    $properties = $this->getPayload()->properties;
    if (isset($properties->cluster_type)) {
      return check_plain($properties->cluster_type);
    }
    return 'pack';
  }

  /**
   * {@inheritdoc}
   */
  protected function getWebpackName() {
    return $this->getVirtualMachineScaleSetName();
  }

}
