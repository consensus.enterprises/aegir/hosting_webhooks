<?php
/**
 * @file
 * The hosting feature definition for hosting_webhooks.
 */

/**
 * Register a hosting feature with Aegir.
 *
 * This will be used to generate the 'admin/hosting' page.
 *
 * Implements hook_hosting_feature().
 *
 * @return array
 *   associative array indexed by feature key.
 */
function hosting_webhooks_hosting_feature() {
  $features['webhooks'] = [
    // Title to display in form.
    'title' => t('Webhook integration'),
    'description' => t('Provides webhook endpoints for some tasks.'),
    // Initial status ( HOSTING_FEATURE_DISABLED, HOSTING_FEATURE_ENABLED, HOSTING_FEATURE_REQUIRED ).
    'status' => HOSTING_FEATURE_DISABLED,
    // Module to enable/disable alongside feature.
    'module' => 'hosting_webhooks',
    // Which group to display in ( null , experimental )
    'group' => 'experimental',
    'role_permissions' => [
      'anonymous user' => [
        'trigger webhook',
      ],
      'aegir administrator' => [
        'administer webhook',
      ],
    ],
    'enable' => 'hosting_webhooks_feature_enable_callback',
  ];

  return $features;
}

