<?php
/**
 * @file
 * hosting_webhooks_server.webhook.inc
 */

/**
 * Implements hook_webhook_default_config().
 */
function hosting_webhooks_server_webhook_default_config() {
  $export = array();

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '1';
  $webhook->title = 'Create Server';
  $webhook->machine_name = 'create_server';
  $webhook->description = 'Webhook for creating Aegir servers.';
  $webhook->unserializer = 'token_authorization';
  $webhook->processor = 'create_server';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['create_server'] = $webhook;

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '2';
  $webhook->title = 'Delete Server';
  $webhook->machine_name = 'delete_server';
  $webhook->description = 'Webhook for deleting Aegir servers.';
  $webhook->unserializer = 'token_authorization';
  $webhook->processor = 'delete_server';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['delete_server'] = $webhook;

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '3';
  $webhook->title = 'Log Server Data';
  $webhook->machine_name = 'log_server_data';
  $webhook->description = 'Webhook for logging incoming Aegir server data.';
  $webhook->unserializer = 'token_authorization';
  $webhook->processor = 'log_server_data';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['log_server_data'] = $webhook;

  return $export;
}
