<?php
/**
 * @file
 * hosting_webhooks_server.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hosting_webhooks_server_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "webhook" && $api == "webhook") {
    return array("version" => "1");
  }
}
