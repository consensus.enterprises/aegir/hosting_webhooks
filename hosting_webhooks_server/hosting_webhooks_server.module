<?php
/**
 * @file
 * Code for the Hosting Webhooks Server feature.
 */

include_once 'hosting_webhooks_server.features.inc';

/**
 * Implements hook_ctools_plugin_directory().
 */
function hosting_webhooks_server_ctools_plugin_directory($module, $type) {
  if ('webhook' == $module) {
    return "plugins/{$type}";
  }
}

/**
 * Implements hook_form_alter().
 */
function hosting_webhooks_server_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id !== 'hosting_webhooks_form') return;

  $form['hosting_webhooks_check_ssh'] = [
    '#type' => 'checkbox',
    '#title' => t('Check that a server is reachable before adding it.'),
    '#default_value' => variable_get('hosting_webhooks_check_ssh', TRUE),
    '#description' => t('Check that server SSH port is reachable before atempting to add a new server node.'),
  ];

  $form['hosting_webhooks_check_ssh_timeout'] = [
    '#type' => 'select',
    '#options' => hosting_webhooks_server_timeout_options(),
    '#title' => t('Server reachability check timeout'),
    '#default_value' => variable_get('hosting_webhooks_check_ssh_timeout', 300),
    '#description' => t('The timeout limit (in seconds) for checking that a server is reachable.'),
  ];
}

/**
 * Return options for timeout form elements.
 */
function hosting_webhooks_server_timeout_options() {
  $options = [
    '1' => 1,
    '5' => 5,
    '10' => 10,
    '20' => 20,
    '30' => 30,
    '60' => 60,
    '300' => 300,
  ];
  return $options;
}

/**
 * Implements hook_webhook_processor().
 */
function hosting_webhooks_server_webhook_processor() {
  $path = drupal_get_path('module', 'hosting_webhooks_server') . '/src';
  $plugins = array();

  $plugins['create_server'] = array(
    'title' => t('Create Server'),
    'processor' => array(
      'path' => $path,
      'file' => 'CreateServerProcessor.php',
      'class' => 'CreateServerProcessor',
    ),
  );

  $plugins['delete_server'] = array(
    'title' => t('Delete Server'),
    'processor' => array(
      'path' => $path,
      'file' => 'DeleteServerProcessor.php',
      'class' => 'DeleteServerProcessor',
    ),
  );

  $plugins['log_server_data'] = array(
    'title' => t('Log Server Data'),
    'processor' => array(
      'path' => $path,
      'file' => 'LogServerDataProcessor.php',
      'class' => 'LogServerDataProcessor',
    ),
  );

  return $plugins;
}
