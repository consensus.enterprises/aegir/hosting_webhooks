<?php
/**
 * @file
 * Webhook processor plugin class.
 */

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/BaseProcessor.php';
include_once drupal_get_path('module', 'hosting_webhooks_server') . '/src/BaseServerTrait.php';

use \HostingWebhooks\BaseServerTrait;
use \HostingWebhooks\BaseProcessor;

/**
 * Webhook that logs incoming Aegir server data.
 */
class LogServerDataProcessor extends BaseProcessor {

  use BaseServerTrait;

}
