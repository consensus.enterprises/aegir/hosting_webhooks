<?php
/**
 * @file
 * Webhook processor plugin class.
 */

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/BaseProcessor.php';
include_once drupal_get_path('module', 'hosting_webhooks_server') . '/src/BaseServerTrait.php';
include_once drupal_get_path('module', 'hosting_webhooks_server') . '/src/CreateServerTrait.php';

use \HostingWebhooks\BaseServerTrait;
use \HostingWebhooks\CreateServerTrait;
use \HostingWebhooks\BaseProcessor;

/**
 * Plugin that creates an Aegir server.
 */
class CreateServerProcessor extends BaseProcessor {

  use BaseServerTrait;
  use CreateServerTrait;

  /**
   * {@inheritdoc}
   */
  protected function doProcessActions() {
    $this->createServer();
  }

}
