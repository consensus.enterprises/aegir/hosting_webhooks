<?php
/**
 * @file
 * Aegir Create Server Trait.
 */

namespace HostingWebhooks;

/**
 * Trait that provides functionality to create Aegir server nodes and services.
 */
trait CreateServerTrait {

  /**
   * Create an Aegir server node.
   */
  protected function createServer() {
    try {
      $this->waitForServerSshPort();
      $server = $this->buildServerNode();
      $this->validateServerNode($server);
    }
    catch(\Exception $e) {
      return $this->logFailure($e->getMessage(), '400 Bad Request');
    }

    $server->server_status = HOSTING_SERVER_QUEUED;
    $server->verified = 0;
    $server->status = NODE_PUBLISHED;

    $this->saveServerNode($server);

    return $server;
  }

  /**
   * Wait until the server's SSH port is reachable.
   */
  protected function waitForServerSshPort() {
    if (!$this->checkServerSshPort()) return;
    if ($this->serverSshPortIsReachable()) return;
    $this->logNotice('Waiting for @hostname SSH port to be reachable.');

    $timeout = variable_get('hosting_webhooks_check_ssh_timeout', 300);
    $timer = 0;
    $increment = 1;
    while (!$this->serverSshPortIsReachable()) {
      if ($timer >= $timeout) {
        return $this->logFailure('Server @hostname SSH port is unreachable.', '408 Request Timeout', 'Timeout waiting for @hostname SSH port to become reachable.');
      }
      sleep($increment);
      $timer += $increment;
    }
  }

  /**
   * Return whether to check a server's SSH port is reachable.
   */
  protected function checkServerSshPort() {
    return variable_get('hosting_webhooks_check_ssh', TRUE);
  }

  /**
   * Wait until the server's SSH port is reachable.
   */
  protected function serverSshPortIsReachable() {
    $host = $this->getServerHostname();
    $port = '22';
    $timeout = '1';
    $fp = @fsockopen($host, $port, $errno, $errstr, $timeout);

    return (boolean) $fp;
  }

  /**
   * Build a server node object.
   */
  protected function buildServerNode() {
    $server = new \stdClass();
    $server->type = 'server';
    $server->title = $this->getServerHostname();
    $server->op = 'create';
    $server->services = $this->buildServerServices();
    return $server;
  }

  /**
   * Return the array of services for the server, based on the webhook payload.
   */
  protected function buildServerServices() {
    $services = [];
    $payload_services = $this->getServicesFromPayload();
    $log_vars = $this->getLogVars();

    foreach (hosting_server_services() as $name => $service) {
      if (!array_key_exists($name, $payload_services)) continue;

      $type = $payload_services->$name;
      $log_vars += [
        '@service' => $service['title'],
        '@type' => $type,
      ];

      if (!array_key_exists($type, $service['types'])) {
        $this->logWarning('Unrecognized @service service type (@type) requested for @hostname.', $log_vars);
        continue;
      }

      $this->logNotice('Adding @type @service service for @hostname.', $log_vars);
      $services[$name] = $this->buildServerService($service, $type);
    }

    return $services;
  }

  /**
   * Return a properly structured service for the server.
   */
  protected function buildServerService($definition, $type) {
    $dummy_node = new \stdClass;
    $service = [
      'type' => $type,
      $type => [],
    ];

    $service_object = new $definition['types'][$type]($dummy_node);
    if ($service_object->has_restart_cmd()) {
      $service[$type]['restart_cmd'] = $service_object->default_restart_cmd();
    }
    if ($service_object->has_port()) {
      $service[$type]['port'] = $service_object->default_port();
    }
    return $service;
  }

  /**
   * Return the array of services defined in the webhook payload.
   */
  protected function getServicesFromPayload() {
    $payload = $this->getPayload();
    if (!isset($payload->services) || empty($payload->services)) {
      $this->logWarning('No services defined for @hostname');
      return [];
    }
    return $payload->services;
  }

  /**
   * Validate the server node, calling any relevant hooks, etc.
   */
  protected function validateServerNode($server) {
    $form = $form_state = [];
    node_validate($server, $form, $form_state);

    // Clear all messages so that they don't display on the next page load.
    drupal_get_messages();

    $errors = form_get_errors();

    if (empty($errors)) return;

    // Clear form error messages so that they don't generate messages.
    form_clear_error();

    foreach ($errors as $error) {
      $message = 'Validation error: @error';
      $this->log($message, ['@error' => $error], WATCHDOG_WARNING);
    }

    throw new \Exception('Server node failed validation.');
  }

  /**
   * Save the server node.
   */
  protected function saveServerNode($server) {
    try {
      node_save($server);
    }
    catch (\Exception $e) {
      return $this->logFailure($e->getMessage());
    }
  }

}
