<?php
/**
 * @file
 * Webhook processor plugin class.
 */

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/BaseProcessor.php';
include_once drupal_get_path('module', 'hosting_webhooks_server') . '/src/BaseServerTrait.php';
include_once drupal_get_path('module', 'hosting_webhooks_server') . '/src/DeleteServerTrait.php';

use \HostingWebhooks\BaseServerTrait;
use \HostingWebhooks\DeleteServerTrait;
use \HostingWebhooks\BaseProcessor;

/**
 * Plugin that deletes an Aegir server.
 */
class DeleteServerProcessor extends BaseProcessor {

  use BaseServerTrait;
  use DeleteServerTrait;

  /**
   * {@inheritdoc}
   */
  protected function doProcessActions() {
    $this->deleteServer();
  }

}
