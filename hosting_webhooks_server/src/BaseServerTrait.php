<?php
/**
 * @file
 * Aegir Base Server Trait.
 */

namespace HostingWebhooks;

/**
 * Trait that provides base functionality for Aegir servers.
 */
trait BaseServerTrait {

  /**
   * Return a single server node based on the hostname.
   */
  protected function getServerNode(string $hostname = '', $get_deleted_server_node = FALSE) {
    // Default to the hostname in the webhook payload.
    if (empty($hostname)) {
      $hostname = $this->getServerHostname();
    }

    try {
      $nodes = $nodes2 = $this->getServerNodes($hostname);
    }
    catch (\Exception $e) {
      throw new \Exception($e->getMessage());
    }

    foreach ($nodes as $nid => $node) {
      if ($node->server_status == HOSTING_SERVER_DELETED) {
        unset($nodes[$nid]);
      }
    }

    if (empty($nodes) && $get_deleted_server_node) {
      return $this->getLastDeletedServerNode($nodes2);
    }

    switch (count($nodes)) {
      case 1:
        break;
      case 0:
        throw new \Exception('No enabled server matched hostname.');
      default:
        throw new \Exception('More than one enabled server matched hostname.');
    }

    return current($nodes);
  }

  /**
   * Return all server nodes based on the hostname.
   */
  protected function getServerNodes(string $hostname) {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'server')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition('title', $hostname);

    $result = $query->execute();

    if (!isset($result['node'])) {
      throw new \Exception('No server matched hostname.');
    }

    return entity_load('node', array_keys($result['node']), [], TRUE);
  }

  /**
   * Return the server hostname from the payload.
   */
  protected function getServerHostname() {
    return check_plain($this->getPayload()->server->hostname);
  }

  /**
   * Return the most recently deleted server node from a given list.
   */
  protected function getLastDeletedServerNode($nodes) {
    foreach ($nodes as $nid => $node) {
      if ($node->server_status !== HOSTING_SERVER_DELETED) {
        unset($nodes[$nid]);
      }
    }
    return end($nodes);
  }

  /**
   * {@inheritdoc}
   */
  protected function getLogVars() {
    return parent::getLogVars() + [
      '@hostname' => $this->getPayload()->server->hostname,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function logTriggered($message = '@plugin webhook plugin triggered for @hostname.') {
    return parent::logTriggered($message);
  }

  /**
   * {@inheritdoc}
   */
  protected function logSuccess($message = '@plugin webhook plugin succeeded for @hostname.') {
    return parent::logSuccess($message);
  }

  /**
   * {@inheritdoc}
   */
  protected function logFailure($exception_message = '', $status = '500 Internal Server Error', $message = '@plugin webhook plugin failed for @hostname.') {
    return parent::logFailure($exception_message, $status, $message);
  }

}
