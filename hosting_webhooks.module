<?php

/**
 * Called when Hosting feature is enabled.
 */
function hosting_webhooks_feature_enable_callback() {
  $message = t(
    'The Hosting Webhooks module has granted the %role role the %perm permission. This is required for proper functioning of webhooks. All the webhooks that ship with this module are secured by an authentication token. Please review any enabled webhooks (!link), and consider their possible security implications.', [
      '%role' => 'anonymous user',
      '%perm' => 'trigger webhook',
      '!link' => '<a href="/admin/config/services/webhook">' . t('link') . '</a>',
    ]
  );
  drupal_set_message($message, 'warning');
}

/**
 * Implements hook_menu().
 */
function hosting_webhooks_menu() {
  $items = [];

  $items['admin/hosting/webhooks'] = [
    'title' => 'Webhooks',
    'description' => 'Configuration of webhooks behaviour.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['hosting_webhooks_form'],
    'access arguments' => ['administer hosting webhooks'],
    'type' => MENU_LOCAL_TASK,
  ];
  $items['admin/hosting/webhooks/settings'] = [
    'title' => 'Settings',
    'description' => 'Configuration of webhooks behaviour.',
    'weight' => -1,
    'type' => MENU_LOCAL_ACTION,
  ];

  return $items;
}

/**
 * Implements hook_permission().
 */
function hosting_webhooks_permission() {
  return [
    'administer hosting webhooks' => [
      'title' => t('Administer Aegir webhook settings'),
    ]
  ];
}

/**
 * Base form page callback for webhooks settings.
 *
 * @see hosting_webhooks_menu()
 */
function hosting_webhooks_form($form, &$form_state) {
  return system_settings_form($form);
}

/**
 * Implements hook_form_alter().
 */
function hosting_webhooks_form_alter(&$form, &$form_state, $form_id) {
  // We only want to alter webhook config forms.
  if ($form_id !== 'ctools_export_ui_edit_item_form') return;
  if (!array_key_exists('whid', $form)) return;
  if (!array_key_exists('unserializer', $form)) return;
  if (!array_key_exists('processor', $form)) return;

  // If this webhook is using our plugin, delegate form customization to the plugin.
  if ($form_state['item']->unserializer !== 'token_authorization') return;
  $plugin = webhook_load_unserializer('token_authorization');
  $plugin->formAlter($form, $form_state);
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function hosting_webhooks_ctools_plugin_directory($module, $type) {
  if ('webhook' == $module) {
    return "plugins/{$type}";
  }
}

/**
 * Implements hook_webhook_unserializer().
 */
function hosting_webhooks_webhook_unserializer() {
  $path = drupal_get_path('module', 'hosting_webhooks') . '/src';
  $plugins = array();

  $plugins['token_authorization'] = array(
    'title' => t('Token Authorization'),
    'unserializer' => array(
      'path' => $path,
      'file' => 'TokenAuthorizationUnserializer.php',
      'class' => 'TokenAuthorizationUnserializer',
    ),
  );

  return $plugins;
}

