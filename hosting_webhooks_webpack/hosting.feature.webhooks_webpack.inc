<?php
/**
 * @file
 * The hosting feature definition for hosting_webhooks_webpack.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_webhooks_webpack_hosting_feature() {
  $features['webhooks_webpack'] = [
    'title' => t('Webhook webpack integration'),
    'description' => t('Provides webhook plugins for webpack tasks.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_webhooks_webpack',
    'group' => 'experimental',
  ];

  return $features;
}
