<?php
/**
 * @file
 * hosting_webhooks_server.webhook.inc
 */

/**
 * Implements hook_webhook_default_config().
 */
function hosting_webhooks_webpack_webhook_default_config() {
  $export = array();

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '1';
  $webhook->title = 'Add Webpack Server';
  $webhook->machine_name = 'add_webpack_server';
  $webhook->description = 'Webhook for adding Aegir servers to a webpack.';
  $webhook->unserializer = 'token_authorization';
  $webhook->processor = 'add_webpack_server';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['add_webpack_server'] = $webhook;

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '2';
  $webhook->title = 'Remove Webpack Server';
  $webhook->machine_name = 'remove_webpack_server';
  $webhook->description = 'Webhook for removing Aegir servers from a webpack.';
  $webhook->unserializer = 'token_authorization';
  $webhook->processor = 'remove_webpack_server';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['remove_webpack_server'] = $webhook;

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '3';
  $webhook->title = 'Log Webpack Data';
  $webhook->machine_name = 'log_webpack_data';
  $webhook->description = 'Webhook for logging incoming Aegir webpack data.';
  $webhook->unserializer = 'token_authorization';
  $webhook->processor = 'log_webpack_data';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['log_webpack_data'] = $webhook;

  return $export;
}
