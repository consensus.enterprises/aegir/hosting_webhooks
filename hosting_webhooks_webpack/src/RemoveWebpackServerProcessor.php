<?php
/**
 * @file
 * Webhook processor plugin class.
 */

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/BaseProcessor.php';
include_once drupal_get_path('module', 'hosting_webhooks_webpack') . '/src/BaseWebpackTrait.php';
include_once drupal_get_path('module', 'hosting_webhooks_webpack') . '/src/RemoveWebpackServerTrait.php';

use \HostingWebhooks\BaseWebpackTrait;
use \HostingWebhooks\RemoveWebpackServerTrait;
use \HostingWebhooks\BaseProcessor;

/**
 * Plugin that removes an Aegir server from a webpack.
 */
class RemoveWebpackServerProcessor extends BaseProcessor {

  use BaseWebpackTrait;
  use RemoveWebpackServerTrait;

  /**
   * {@inheritdoc}
   */
  protected function doProcessActions() {
    $this->removeServerFromWebpack();
  }

}
