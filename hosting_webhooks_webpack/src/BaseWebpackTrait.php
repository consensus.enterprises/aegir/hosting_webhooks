<?php
/**
 * @file
 * Aegir Base Webpack Trait.
 */

namespace HostingWebhooks;

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks_server') . '/src/BaseServerTrait.php';
include_once drupal_get_path('module', 'hosting_webhooks_server') . '/src/CreateServerTrait.php';
include_once drupal_get_path('module', 'hosting_webhooks_server') . '/src/DeleteServerTrait.php';

use \HostingWebhooks\BaseServerTrait;
use \HostingWebhooks\CreateServerTrait;
use \HostingWebhooks\DeleteServerTrait;

/**
 * Trait that provides base functionality for Aegir webpacks.
 */
trait BaseWebpackTrait {

  use BaseServerTrait;
  use CreateServerTrait;
  use DeleteServerTrait;

  /**
   * Return the webpack node.
   */
  protected function getWebpackNode() {
    return $this->getServerNode($this->getWebpackName());
  }

  /**
   * Return the webpack name from the payload.
   */
  protected function getWebpackName() {
    return check_plain($this->getPayload()->cluster->name);
  }

  /**
   * Return the array of services for the server, based on the webhook payload.
   */
  protected function buildWebpackServices($webpack) {
    $services = [];
    $type = $this->getClusterType();
    $services['http'] = [
      'type' => $type,
      $type => [
        'port' => 0,
        'master_servers' => $this->getMasterServers($webpack),
        'slave_servers' => $this->getMinionServers($webpack),
      ],
    ];
    return $services;
  }

  /**
   * Return the cluster type from the payload.
   */
  protected function getClusterType() {
    return check_plain($this->getPayload()->cluster->type);
  }

  /**
   * Return the NIDs of the master servers for the cluster.
   */
  protected function getMasterServers($cluster) {
    $type = $this->getClusterType();
    foreach ($cluster->services as $name => $service) {
      if ($name !== 'http') continue;
      if ($service->type !== $type) continue;
      return $service->master_servers;
    }

    // At this point, we just hardcode the Aegir hostmaster server as the default.
    // @TODO: Make this configurable from the payload?
    $hostmaster_platform = node_load(hosting_get_hostmaster_platform_nid());
    $nid = $hostmaster_platform->web_server;

    return [$nid => $nid];
  }

  /**
   * Return the NIDs of the minion servers for the cluster.
   */
  protected function getMinionServers($cluster) {
    $type = $this->getClusterType();
    foreach ($cluster->services as $name => $service) {
      if ($name !== 'http') continue;
      if ($service->type !== $type) continue;
      return $service->slave_servers;
    }
    return [];
  }

  /**
   * Get the hostnames of servers in a webpack cluster.
   */
  protected function getMinionHostnames() {
    $webpack = $this->getWebpack();
    $servers = [];
    foreach ($this->getMinionServers($webpack) as $nid) {
      $server = node_load($nid);
      if (!is_object($server)) continue;
      $servers[] = $server->title;
    }

    return $servers;
  }

  /**
   * {@inheritdoc}
   */
  protected function getLogVars() {
    $payload = $this->getPayload();
    return parent::getLogVars() + [
      '@hostname' => $payload->server->hostname,
      '@cluster_name' => $payload->cluster->name,
      '@cluster_type' => $payload->cluster->type,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function logTriggered($message = '@plugin webhook plugin triggered for @hostname with @cluster_type cluster @cluster_name.') {
    return parent::logTriggered($message);
  }

  /**
   * {@inheritdoc}
   */
  protected function logSuccess($message = '@plugin webhook plugin succeeded for @hostname with @cluster_type cluster @cluster_name.') {
    return parent::logSuccess($message);
  }

  /**
   * {@inheritdoc}
   */
  protected function logFailure($exception_message = '', $status = '500 Internal Server Error', $message = '@plugin webhook plugin failed for @hostname with @cluster_type cluster @cluster_name.') {
    return parent::logFailure($exception_message, $status, $message);
  }

}
