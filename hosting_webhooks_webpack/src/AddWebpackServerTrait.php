<?php
/**
 * @file
 * Aegir Add Webpack Server Trait.
 */

namespace HostingWebhooks;

/**
 * Trait that provides functionality to create Aegir server nodes and services.
 */
trait AddWebpackServerTrait {

  /**
   * Add an Aegir server to a webpack cluster.
   */
  protected function addServerToWebpack() {
    try {
      $server = $this->getServerToAdd();
      $webpack = $this->getWebpack();
      $this->registerServerWithWebpack($server, $webpack);
    }
    catch (\Exception $e) {
      return $this->logFailure($e->getMessage(), '500 Internal Server Error', 'Could not add server to webpack.');
    }
  }

  /**
   * Return the server to add to the webpack, creating a new one if it doesn't already exist.
   */
  protected function getServerToAdd() {
    try {
      $server = $this->getServerNode();
    }
    catch (\Exception $e) {
      $message = $e->getMessage();
      if ($message == 'More than one enabled server matched hostname.') {
        return $this->logFailure($message, '409 Conflict', 'Could not determine which server to add to webpack.');
      }
      $server = $this->createServer();
    }

    return $this->getEnabledServer($server);
  }

  /**
   * Wait for a given server to be enabled (verified).
   */
  protected function getEnabledServer(\stdClass $server) {
    // If the server is already enabled, just return it.
    if ($this->serverIsEnabled($server)) return $server;

    // If we have an existing server, that's not enabled, then it failed
    // verification previously. Trigger a new verify task on it.
    if (!$this->serverIsNew($server)) {
      $this->verifyServer($server);
    }

    $timeout = variable_get('hosting_webhooks_verify_timeout', 300);
    $timer = 0;
    while (!$this->verifyTaskIsComplete($server)) {
      if ($timer >= $timeout) {
        return $this->logFailure('Timeout waiting for server to be verified.', '408 Request Timeout', 'Could not determine which server to add to webpack.');
      }
      sleep(1);
      $server = $this->getServerNode();
      $timer++;
    }

    if ($this->serverIsEnabled($server)) return $server;

    return $this->logFailure('Server verification failed.', '424 Failed Dependency', 'The @hostname server failed verification.');
  }

  /**
   * Check if a given server node is enabled.
   */
  protected function serverIsEnabled(\stdClass $server) {
    // Newly created server nodes aren't enabled yet.
    if ($this->serverIsNew($server)) return FALSE;

    return $server->server_status === HOSTING_SERVER_ENABLED;
  }

  /**
   * Check if a given server node was newly created.
   */
  protected function serverIsNew(\stdClass $server) {
    if (!isset($server->server_status)) return TRUE;
    return $server->server_status < HOSTING_SERVER_ENABLED;
  }

  /**
   * Trigger a new verify task on an existing server.
   */
  protected function verifyServer(\stdClass $server) {
    // @TODO ensure the services are up-to-date with the current payload? Maybe they've changed?
    hosting_add_task($server->nid, 'verify');
  }

  /**
   * Determine whether the most recent 'verify' task is complete for a given server.
   */
  protected function verifyTaskIsComplete(\stdClass $server) {
    $task = hosting_get_most_recent_task($server->nid, 'verify');
    return $task->task_status > HOSTING_TASK_QUEUED;
  }

  /**
   * Return the webpack, creating a new one if it doesn't already exist.
   */
  protected function getWebpack() {
    try {
      $webpack = $this->getWebpackNode();
    }
    catch (\Exception $e) {
      $message = $e->getMessage();
      if ($message == 'More than one server matched hostname.') {
        return $this->logFailure($message, '409 Conflict', 'Could not determine which webpack with which to register server.');
      }
      $webpack = $this->createWebpack();
    }
    return $webpack;
  }

  /**
   * Create an Aegir webpack node.
   */
  protected function createWebpack() {
    $webpack = $this->buildWebpackNode();

    try {
      // Webpacks are server nodes in Aegir, so we can re-use the validation method.
      $this->validateServerNode($webpack);
    }
    catch(\Exception $e) {
      return $this->logFailure('Webpack node failed validation.', '400 Bad Request');
    }

    $webpack->status = NODE_PUBLISHED;

    // Webpacks just nodes in Aegir, so we can save them like any other server node.
    $this->saveServerNode($webpack);

    return $webpack;
  }

  /**
   * Build a webpack node object.
   */
  protected function buildWebpackNode() {
    $name = $this->getWebpackName();
    $webpack = new \stdClass();
    $webpack->type = 'server';
    $webpack->title = $name;
    $webpack->op = 'create';
    $webpack->server_status = HOSTING_SERVER_QUEUED;
    $webpack->verified = 0;
    $webpack->human_name = $name;

    // Don't register the webpack service until we have a minion server to add.
    $webpack->services = [];
    return $webpack;
  }

  /**
   * Add a minion server to a webpack cluster.
   */
  protected function registerServerWithWebpack($server, $webpack) {
    if ($this->serverInWebpack($server, $webpack)) return;

    $services = $this->buildWebpackServices($webpack);
    $type = $this->getClusterType();
    $services['http'][$type]['slave_servers'][$server->nid] = $server->nid;
    $webpack->services = $services;
    $this->saveServerNode($webpack);
  }

  /**
   * Check is a given minion server is already in a webpack cluster.
   */
  protected function serverInWebpack($server, $webpack) {
    return array_key_exists($server->nid, $this->getMinionServers($webpack));
  }

}
