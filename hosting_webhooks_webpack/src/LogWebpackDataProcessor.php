<?php
/**
 * @file
 * Webhook processor plugin class.
 */

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/BaseProcessor.php';
include_once drupal_get_path('module', 'hosting_webhooks_webpack') . '/src/BaseWebpackTrait.php';

use \HostingWebhooks\BaseWebpackTrait;
use \HostingWebhooks\BaseProcessor;

/**
 * Webhook that logs incoming Aegir webpack data.
 */
class LogWebpackDataProcessor extends BaseProcessor {

  use BaseWebpackTrait;

}
