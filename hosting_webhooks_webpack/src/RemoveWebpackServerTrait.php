<?php
/**
 * @file
 * Aegir Remove Server Trait.
 */

namespace HostingWebhooks;

/**
 * Trait that provides functionality to remove Aegir servers from a webpack.
 */
trait RemoveWebpackServerTrait {

  /**
   * Remove an Aegir server from a webpack.
   */
  protected function removeServerFromWebpack() {
    try {
      $get_deleted_server_node = TRUE;
      $server = $this->getServerNode('', $get_deleted_server_node);
      $webpack = $this->getWebpackNode();
      $this->unregisterServerWithWebpack($server, $webpack);
      $this->deleteServer();
    }
    catch (\Exception $e) {
      return $this->logFailure($e->getMessage(), '500 Internal Server Error', 'Could not remove server from webpack.');
    }
  }

  /**
   * Remove a minion server from a webpack cluster.
   */
  protected function unregisterServerWithWebpack($server, $webpack) {
    $services = $this->buildWebpackServices($webpack);
    $type = $this->getClusterType();
    unset($services['http'][$type]['slave_servers'][$server->nid]);
    $webpack->services = $services;
    $this->saveServerNode($webpack);
  }

}
