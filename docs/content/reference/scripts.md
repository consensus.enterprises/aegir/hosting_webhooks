---
title: Scripts & Fixtures
weight: 40

---

### Test Scripts

The following scripts are used in testing:
* [`github.sh`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/github.sh): This script uses cURL to trigger the default example webhook.
* [`logdata.sh`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/logdata.sh): This script triggers a basic webhook to demonstrate logging capabilities.
* [`server.sh`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/server.sh): This script is used extensively to test server, services and webpack webhooks.
* [`tokenauth.sh`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/tokenauth.sh): This script is used to test the security of the authorization token mechanism.
* [`delete_servers.php`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/delete_servers.php): This script deletes all test server nodes, and resets to defult settings.


### Fixtures

The following fixtures are used in testing:
* [`server_drupalwebpack.alias.drushrc.php`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/server_drupalwebpack.alias.drushrc.php): This Drush alias enables the creation of a test webpack cluster fixture during tests.
* [`server_testhost.alias.drushrc.php`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/server_testhost.alias.drushrc.php): This Drush alias enables the creation of a test server fixture during tests.
* [`server_webpackhost.alias.drushrc.php`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/server_webpackhost.alias.drushrc.php): This Drush alias enables the creation of a test server fixture during tests.
* [`azure_scalein_payload.json`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/azure_scalein_payload.json): This webhook payload simulates an Azure scale in operation webhook.
* [`azure_scaleout_payload.json`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/azure_scaleout_payload.json): This webhook payload simulates an Azure scale out operation webhook.
* [`azure_server_data_payload.json`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/azure_server_data_payload.json): This webhook payload simulates an Azure scale in operation using a test account, to test polling the Azure VMSS API.
* [`github_payload.json`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/github_payload.json): This webhook payload simulates a Github post-receive webhook.
* [`logdata_payload.json`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/logdata_payload.json): This webhook payload is used to test logging functionality.
* [`serviceshost_payload.json`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/serviceshost_payload.json): This webhook payload is used to test enabling of services on servers.
* [`testhost_payload.json`](ihttps://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/testhost_payload.json): This webhook payload is used to test the creation and deletion of servers.
* [`tokenauth_payload.json`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/tokenauth_payload.json): This webhook payload is used to test the token authorization functionality.
* [`web0_payload.json`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/web0_payload.json): This webhook payload ensures that servers designated by FQDNs are handled correctly.
* [`web1_payload.json`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/web1_payload.json): This webhook payload simulates servers designated by FQDNs are handled correctly.
* [`web2_payload.json`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/scripts/web2_payload.json): This webhook payload ensure that servers designated by IP addresses are handled correctly.
* [`webpackhost_payload.json`](): This webhook payload ensures that webpacks are created properly, and that servers can be added to them.
