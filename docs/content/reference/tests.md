---
title: Tests reference
weight: 40

---

Basic tests:
* [testing.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/testing.feature): Smoke test to ensure that testing components are working as expected.

Webhooks tests:
* [github-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/github-webhook.feature): Smoke test to ensure that the `webhook` module itself is working as expected.
* [logdata-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/logdata-webhook.feature): Ensure that our logging facility is working properly.
* [tokenauth-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/tokenauth-webhook.feature): Test that our token authorization plugin secures webhooks.

Server tests:
* [log-server-data-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/log-server-data-webhook.feature): Ensure that we can log server-related webhook data.
* [create-server-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/create-server-webhook.feature): Ensure that we can create servers.
* [create-services-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/create-services-webhook.feature): Ensure that we can enable services on servers.
* [delete-server-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/delete-server-webhook.feature): Ensure that we can delete servers.
* [vagrant-server-webhooks.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/vagrant-server-webhooks.feature): Run end-to-end server tests by spinning up and down Vagrant VMs.

Webpack tests:

* [log-webpack-data-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/log-webpack-data-webhook.feature): Ensure that we can log webpack-related webhook data.
* [add-webpack-server-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/add-webpack-server-webhook.feature): Ensure that we can create web pack clusters and add servers to them.
* [remove-webpack-server-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/remove-webpack-server-webhook.feature): Ensure that we can remove servers from web pack clusters.
* [vagrant-webpack-webhooks.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/vagrant-webpack-webhooks.feature): Run end-to-end webpack tests by spinning up and down Vagrant VMs.

Azure tests:
* [log-azure-scalein-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/log-azure-scalein-webhook.feature) Ensure that we can handle Azure "scale in" webhooks properly.
* [log-azure-scaleout-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/log-azure-scaleout-webhook.feature) Ensure that we can handle Azure "scale out" webhooks properly.
* [log-azure-server-data-webhook.feature](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/features/log-azure-server-data-webhook.feature): Ensure that can log Azure server webhook data.
