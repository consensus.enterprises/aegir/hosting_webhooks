---
title: BaseProcessor
slug: base-processor

---

The [BaseProcessor](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/src/BaseProcessor.php) class is an abstract class used as a basis for all other webhook processors across the project. It provides very little functionality on its own.

It uses the [ProcessorTrait](../processor-trait) trait.

It is extended by the following clases:
* [LogDataProcessor](../../hosting_webhooks_example/log-data-processor)
* [AddWebpackServerProcessor](../../hosting_webhooks_webpack/add-webpack-server-processor)
* [RemoveWebpackServerProcessor](../../hosting_webhooks_webpack/remove-webpack-server-processor)
* [LogWebpackDataProcessor](../../hosting_webhooks_webpack/log-webpack-data-processor)
* [DeleteServerProcessor](../../hosting_webhooks_server/delete-server-processor)
* [CreateServerProcessor](../../hosting_webhooks_server/create-server-processor)
* [LogServerDataProcessor](../../hosting_webhooks_server/log-server-data-processor)
* [LogAzureDataProcessor](../../hosting_webhooks_azure/log-azure-data-processor)
* [AzureScaleSetProcessor](../../hosting_webhooks_azure/azure-scale-set-processor)
* [LogAzureServerDataProcessor](../../hosting_webhooks_azure/log-azure-server-data-processor)
