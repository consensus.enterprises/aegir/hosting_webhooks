---
title: hosting_webhooks
weight: 10

---

This [module](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/tree/7.x-1.x/hosting_webhooks) provides implementations of webhooks to trigger actions in Aegir.

{{% children %}}
