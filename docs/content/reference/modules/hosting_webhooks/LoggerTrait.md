---
title: LoggerTrait
slug: logger-trait

---

The [LoggerTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/src/LoggerTrait.php) trait provides re-usable functionality related to logging data to the database.

It is used in the following class:
* [BaseProcessor](../base-processor)
