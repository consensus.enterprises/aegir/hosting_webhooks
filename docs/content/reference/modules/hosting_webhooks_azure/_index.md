---
title: hosting_webhooks_azure
weight: 50

---

This [module](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/tree/7.x-1.x/hosting_webhooks_azure) provides webhooks for adding and removing Aegir servers based on Azure scale set operations.

{{% children %}}
