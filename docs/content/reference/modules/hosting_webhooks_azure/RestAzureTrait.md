---
title: RestAzureTrait
slug: rest-azure-trait

---

The [RestAzureTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_azure/src/RestAzureTrait.php) trait provides re-usable functionality related to making REST calls to the Azure API.

It uses the [OAuth2AzureTrait](../oauth2-azure-trait) trait.

It is used in the following trait:
* [ScaleSetAzureTrait](../scale-set-azure-trait)
