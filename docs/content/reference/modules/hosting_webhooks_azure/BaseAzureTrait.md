---
title: BaseAzureTrait
slug: base-azure-trait

---

The [BaseAzureTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_azure/src/BaseAzureTrait.php) trait provides base functionality for Azure scale set operations.

It uses the [BaseWebpackTrait](../../hosting_webhooks_webpack/base-webpack-trait), [AddWebpackServerTrait](../../hosting_webhooks_webpack/add-webpack-server-trait) and [RemoveWebpackServerTrait](../../hosting_webhooks_webpack/remove-webpack-server-trait) traits.

It is used in the following classes and traits:
* [WebpackAzureTrait](../webpack-azure-trait)
