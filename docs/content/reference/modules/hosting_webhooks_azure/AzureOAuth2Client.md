---
title: AzureOAuth2Client
slug: azure-oauth2-client

---

The [AzureOAuth2Client](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_azure/src/AzureOAuth2Client.php) class communicates with the Azure API servers, to retrieve a token used to authenticate REST calls to the Azure API.

It extends the [OAuth2/Client](https://git.drupalcode.org/project/oauth2_client/-/blob/7.x-2.x/src/Client.php) class, and adds a `resource` URL parameter to the OAuth2 query. This is required by the Azure API to define the scope of the resulting authentication token.

It is used by the [OAuth2AzureTrait](../oauth2-azure-trait) trait.
