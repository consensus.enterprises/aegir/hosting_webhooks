---
title: AzureScaleSetProcessor
slug: azure-scale-set-processor

---

The [AzureScaleSetProcessor](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_azure/src/AzureScaleSetProcessor.php) class is a webhook processor that integrates Azure scale sets with Aegir web pack clusters. It will create a web pack cluster named after the scale set, and register any VMs in the scale set as Aegir server minions.

It extends the [BaseProcessor](../../hosting_webhooks/base-processor) class, and uses the [WebpackAzureTrait](../webpack-azure-trait).
