---
title: WebpackAzureTrait
slug: webpack-azure-trait

---

The [WebpackAzureTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_azure/src/WebpackAzureTrait.php) trait provides re-usable functionality related to Azure scale sets and Aegir web pack clusters.

It uses the [ScaleSetAzureTrait](../scale-set-azure-trait) and [BaseAzureTrait](../base-azure-trait) traits.

It is used in the following classes:
* [LogAzureDataProcessor](../log-azure-data-processor)
* [AzureScaleSetProcessor](../azure-scale-set-processor)
* [LogAzureServerDataProcessor](../log-azure-server-data-processor)
