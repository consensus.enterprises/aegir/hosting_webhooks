---
title: OAuth2AzureTrait
slug: oauth2-azure-trait

---

The [OAuth2AzureTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_azure/src/OAuth2AzureTrait.php) trait provides re-usable OAuth2 functionality for Azure scale set operations.

It uses the [AzureOAuth2Client](../azure-oauth2-client) class.

It is used in the following trait:
* [RestAzureTrait](../rest-azure-trait)
