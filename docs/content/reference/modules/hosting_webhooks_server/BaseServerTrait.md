---
title: BaseServerTrait
slug: base-server-trait

---

The [BaseServerTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_server/src/BaseServerTrait.php) trait provides functionality shared between [CreateServerProcessor](../create-server-processor) and [DeleteServerProcessor](../delete-server-processor).

It is used in the following classes and traits:
* [CreateServerProcessor](../create-server-processor)
* [DeleteServerProcessor](../delete-server-processor)
* [LogServerDataProcessor](../log-server-data-processor)
* [BaseWebpackTrait](../../hosting_webhooks_webpack/base-webpack-trait)
