---
title: hosting_webhooks_server
weight: 30

---

This [module](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/tree/7.x-1.x/hosting_webhooks_server) provides webhooks and re-useable code for creating and deleting Aegir servers.

{{% children %}}
