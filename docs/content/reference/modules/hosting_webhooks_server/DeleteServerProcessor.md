---
title: DeleteServerProcessor
slug: delete-server-processor

---

The [DeleteServerProcessor](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_server/src/DeleteServerProcessor.php) class is a webhook processor that deletes Aegir server nodes.

It extends the [BaseProcessor](../../hosting_webhooks/base-processor) class, and uses the [BaseServerTrait](../base-server-trait) and [DeleteServerTrait](../delete-server-trait) traits.

It is covered by the following tests:
* [vagrant-server-webhooks.feature](../../../tests/vagrant-server-webhooks)
* [delete-server-webhook.feature](../../../tests/delete-server-webhook)
