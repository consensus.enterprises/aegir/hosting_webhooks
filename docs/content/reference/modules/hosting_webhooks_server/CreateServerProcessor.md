---
title: CreateServerProcessor
slug: create-server-processor

---

The [CreateServerProcessor](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_server/src/CreateServerProcessor.php) class is a webhook processor that creates an Aegir server node and enables services on it.

It extends the [BaseProcessor](../../hosting_webhooks/base-processor) class, and uses the [BaseServerTrait](../base-server-trait) and [CreateServerTrait](../create-server-trait) traits.

It is covered by the following tests:
* [vagrant-server-webhooks.feature](../../../tests/vagrant-server-webhooks)
* [create-server-webhook.feature](../../../tests/create-server-webhook)
* [create-services-webhook.feature](../../../tests/create-services-webhook)
