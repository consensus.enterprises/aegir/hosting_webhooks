---
title: Drush Extension
weight: 10

---

This [Drush extention](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_server/drush/hosting_webhooks_server.drush.inc) implements Provision hooks to add and remove servers from the `aegir` user's `known_hosts` file. This enables automated SSH access without manual intervention.
