---
title: LogServerDataProcessor
slug: log-server-data-processor

---

The [LogServerDataProcessor](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_server/src/LogServerDataProcessor.php) class is a webhook processor that logs server data. It is intended to be used to debug production environments without initiating any Hosting tasks.

It extends the [BaseProcessor](../../hosting_webhooks/base-processor) class, and uses the [BaseServerTrait](../base-server-trait) trait.

It is covered by the following test:
* [log-server-data-webhook.feature](../../../tests/log-server-data-webhook)
