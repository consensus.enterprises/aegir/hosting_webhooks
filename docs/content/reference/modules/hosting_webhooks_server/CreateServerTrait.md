---
title: CreateServerTrait
slug: create-server-trait

---

The [CreateServerTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_server/src/CreateServerTrait.php) trait provides re-usable functionality to create Aegir server nodes and services.

It is used in the following classes and traits:
* [CreateServerProcessor](../create-server-processor)
* [BaseWebpackTrait](../../hosting_webhooks_webpack/base-webpack-trait)
