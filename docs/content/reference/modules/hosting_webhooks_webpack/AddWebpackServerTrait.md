---
title: AddWebpackServerTrait
slug: add-webpack-server-trait

---

The [AddWebpackServerTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_webpack/src/AddWebpackServerTrait.php) trait provides re-usable functionality related to adding Aegir servers to webpack clusters.

It is used in the following classes and traits:
* [AddWebpackServerProcessor](../add-webpack-server-processor)
* [BaseAzureTrait](../../hosting_webhooks_azure/base-azure-trait)
