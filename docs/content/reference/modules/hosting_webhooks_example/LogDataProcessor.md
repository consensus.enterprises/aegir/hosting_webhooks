---
title: LogDataProcessor
slug: log-data-processor

---

The [LogDataProcessor](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_example/src/LogDataProcessor.php) class is a webhook processor that logs webhook data. It is intended as a basic example of a minimal processor implemented with this project's API.

It extends the [BaseProcessor](../../hosting_webhooks/base-processor) class.

It is covered by the following test:
* [logdata-webhook.feature](../../../tests/logdata-webhook)
