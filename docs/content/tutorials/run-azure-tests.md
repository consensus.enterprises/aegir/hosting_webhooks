---
title: Run Azure tests
weight: 42

---

Once you have an Aegir [test server provisioned and
configured](/tutorials/setup-azure-test-environment), configured it with your [Azure credentials](/how-to/setup-azure-credentials#configuring-azure-credentials-on-the-aegir-front-end), and have [configured the
Azure environment to send webhooks to it](/tutorials/configure-azure-webhooks),
you are ready to test.

The following steps will demonstrate that, when the configured scaleset scales
in or scales out,

  1. Azure is calling the Aegir webhook, and 
  1. Aegir is adding and removing Server nodes in response.

## Scale Out

  1. Verify the number of Server nodes known to the Aegir server matches the number of scale set members indicated in the Azure front end.
  ![One server](/images/manual-testing/aegir-servers-1.png)
  1. On the Azure front-end, trigger a "scale out" operation by increasing the default capacity of the scale set (for example, by changing it from 1 to 2 in the text box indicated in red here): 
  ![Azure portal scale set](/images/manual-testing/azure-portal-scale-set-1.png)
  1. Wait a few minutes for the operation to complete.
  1. Observe that the Azure front-end Instances view updates to indicate that a new scale set instance has been provisioned.
  ![Azure portal provisioning succeeded](/images/manual-testing/azure-portal-provisioning-succeeded.png)
  1. On the Aegir front-end, navigate to the recent log messages by clicking Reports in the menu at the top of the screen, and selecting Recent Log Messages.
  ![Aegir reports](/images/manual-testing/aegir-reports.png)
  1. Observe that a scale out webhook was received by Aegir, that it then waited for the new scale set instance to be provisioned and for SSH to be available.
  ![Aegir webhook triggered](/images/manual-testing/aegir-scale-out-plugin-triggered-01.png)
  ![Aegir payload received](/images/manual-testing/aegir-scale-out-received-payload-02.png)
  ![Aegir manual-testing completed](/images/manual-testing/aegir-scale-out-completed-03.png)
  ![Aegir discovered IP](/images/manual-testing/aegir-scale-out-discovered-ip-04.png)
  ![Aegir waiting for SSH](/images/manual-testing/aegir-scale-out-waiting-for-ssh-05.png)
  ![Aegir adding Apache service](/images/manual-testing/aegir-scale-out-adding-apache-service-06.png)
  1. Observe that Aegir then added the new scale set instance as an Apache Server, and logged successful processing of the webhook.
  ![Aegir webhook succeeded](/images/manual-testing/aegir-scale-out-succeeded-07.png)
  1. Observe that the new Server now appears in Aegir's list of Servers.
  1. Observe, on the same page, that Aegir completes a "Verify" for the new Server IP address, and the scaleset cluster as a whole, indicating that it has:
      * confirmed it can SSh to the new scale set instance, and
      * confirmed permissions are correct on the platform (web site) and config directories there.
  ![Aegir new Server](/images/manual-testing/aegir-scale-out-servers-08.png)

## Scale In

  1. On the Azure front-end, trigger a "scale in" operation by decreasing the default capacity of the scale set (for example, from 2 to 1, in the text box indicated here): 
  ![Azure portal scale set](/images/manual-testing/azure-portal-scale-set-1.png)
  1. Wait a few minutes for the operation to complete.
  1. Observe that the Azure front-end Instances view updates to indicate that a scale set instance has been removed.
  ![Azure portal scale set instance removed](/images/manual-testing/azure-portal-scale-in.png)
  1. On the Aegir front-end, navigate to the recent log messages by clicking Reports in the menu at the top of the screen, and selecting Recent Log Messages.
  ![Aegir reports](/images/manual-testing/aegir-reports.png)
  1. Observe that: 
      * a scale in webhook was received by Aegir,
      * that it then waited for the scale set operation to complete, 
      * that it then determined which VM had been removed by discovering which IP address was now missing,
      * and that it then indicated the webhook processing had succeeded.
  ![Aegir scale in plugin triggered](/images/manual-testing/aegir-scale-in-plugin-triggered-01.png)
  ![Aegir scale in payload received](/images/manual-testing/aegir-scale-in-received-payload-02.png)
  ![Aegir scale in waiting](/images/manual-testing/aegir-scale-in-waiting-03.png)
  ![Aegir scale in operation completed](/images/manual-testing/aegir-scale-in-operation-completed-04.png)
  ![Aegir scale in discovered missing IP](/images/manual-testing/aegir-scale-in-discovered-missing-05.png)
  ![Aegir scale in operation completed](/images/manual-testing/aegir-scale-in-succeeded-06.png)
  1. Observe that the Server that was removed no longer appears in Aegir's list of Servers, and that Aegir has completed a Delete task on the IP address in question.
  ![Aegir scale in Servers](/images/manual-testing/aegir-scale-in-servers-1.png)

