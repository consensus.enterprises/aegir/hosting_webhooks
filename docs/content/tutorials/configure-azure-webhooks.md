---
title: Configure Azure webhooks
weight: 41

---

Once you have [set up an Aegir server and scale set in an Azure test environment](/tutorials/setup-azure-test-environment) and [entered your Azure credentials in Aegir](/how-to/setup-azure-credentials/#configuring-azure-credentials-on-the-aegir-front-end), you need to configure Azure to call scale set operation webhooks on your Aegir server before proceeding to [manually test](/tutorials/run-azure-tests).

***Prerequisite:*** The front-end URL of the Aegir server you created previously must be routable via DNS. You can set this up in your own DNS server, or Azure's, as appropriate.

  1. On the Aegir front-end, navigate to the configuration page for webhooks by clicking Configuration > Web Services > Webhooks in the menu at the top of the screen.
  ![Aegir Webhooks Configuration](/images/manual-testing/integration-webhooks-config-navigation.png)
  1. On the Webhooks configuration screen, click on the pulldown menu to the right of the `azure_scale_set` example configuration and select `Clone`. **Note: in some situations, cloning the example configuration may not work due to a Drupal core bug. If this is the case for you, simply edit the example configuration and rename it appropriately, instead of cloning it.**
  ![Clone Aegir Webhook Configuration](/images/manual-testing/integration-clone-config.png)
  1. By default, Aegir calls the new configuration `clone_of_azure_scale_set.` Rename it to something more appropriate; it needs to be something that can appear in a URL, so using lowercase and underscores is a safe bet. In our example, we will use the name of our example scale set in Azure (`aegir_webhook_test_scaleset`).
  ![Edit Aegir Webhook Configuration](/images/manual-testing/integration-edit-config.png)
  1. Scroll down and click the Generate Token button to generate a new token (it is insecure to use the default one).
  ![Generate Aegir Webhook Token](/images/manual-testing/integration-generate-token.png)
  1. Next, on the Azure front-end, navigate to your scale set and click on the Notify tab, as shown here:
  ![Click Notify tab](/images/manual-testing/integration-azure-notify-tab.png)
  1. Fill in the webhook URL with the DNS routable URL of your Aegir server, and the webhook config name you provided above, plus the token you generated, as shown here, then hit Save above:
  ![Fill in token](/images/manual-testing/integration-azure-enter-url-and-token.png)


You are now ready to [manually test](/tutorials/run-azure-tests).
