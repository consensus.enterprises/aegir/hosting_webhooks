---
title: Set up Azure test environment
weight: 40

---

The [`aegir-webhook-dev-vm`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm) project provides [example code](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/tree/master/azure) for setting up a live Aegir webhook test environment with a scale set in Azure. Please note this example is not production-ready.

Features of the example code include:

* Creation of images (using Packer and Ansible) for both the webpack nodes and the Aegir server.
* Deployment of the Aegir server and a webpack node scale set, using Azure CLI tools.

### Requirements

* [Ubuntu 18.04](https://releases.ubuntu.com/18.04/)
* [Azure CLI tools](https://docs.microsoft.com/en-us/cli/azure/)
* [Packer](https://packer.io)
* [Ansible 2.9.x](https://docs.ansible.com)
* [GNU Make](https://www.gnu.org/software/make/)
* [jq](https://stedolan.github.io/jq/)

### Dependencies

* Ensure you have cloned the `aegir-webhooks-dev-vm` code as described [here](/tutorials/setup-local-dev-environment).
* Ensure that you have [set up your Azure CLI credentials](/how-to/setup-azure-credentials/#configuring-azure-credentials-on-the-command-line). 

### Steps to set up the test environment

##### 1. Start in the `azure` directory:
```
$ cd aegir-webhooks-dev-vm/azure
```

#####  2. Create a `.secret/azure-secret.json` file with the following 4 pieces of information in it:
```
{
  "tenant_id": "<YOUR TENANT ID HERE>",
  "client_id": "<YOUR CLIENT ID HERE>",
  "client_secret": "<YOUR CLIENT SECRET HERE>",
  "subscription_id": "<YOUR SUBSCRIPTION ID HERE>"
}
```

#####  3. Build the Aegir server image: 
```
make aegir-image
```
  This step uses Packer to build a VM image in Azure that stops short of the "Site Install" stage (which has to be done per-server). It also creates an SSH key for the aegir user, which it downloads into the `.secret` directory.

##### 4. Build webpack node image: 
```
make minion-image
``` 
  This step uses Packer to build a VM image in Azure, using the [consensus.aegir-minion](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir-minion) Ansible role. It also inserts the Aegir server SSH key from the previous step into the minion `aegir` user's `authorized_keys`.

##### 5. Deploy the Aegir server: 
```
make aegir-server
```
  This provisions a server based on the Aegir server image created above in step 3. It finishes the Aegir hostmaster site install, enables extra modules, and should output a login link when done. You can repeat this step should you ever need to rebuild the Aegir server.

##### 6. Build the Scale Set: 
```
make scaleset
``` 
  Creates a scale set with public IP per VM, using the webpack node image created in step 4 above. Ensure you navigate to the Azure front-end and verify your scale set has been created. In particular, ensure it is set to auto-scale (not manual).

#### Next Steps

You are now ready to:

 * Configure Aegir with your [Azure credentials](/how-to/setup-azure-credentials/#configuring-azure-credentials-on-the-aegir-front-end).
 * [configure Azure to call webhooks on your Aegir server](/tutorials/configure-azure-webhooks), and then
 * [manually test Azure webhooks](/tutorials/run-azure-tests).

Optionally, you may also [explore other make targets available in the example code](/reference/make-targets).

