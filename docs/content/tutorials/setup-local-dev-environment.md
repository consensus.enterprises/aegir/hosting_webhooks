---
title: Set up local development environment
weight: 30

---

### Prerequisites

* git
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/docs/installation)

### Steps to set up a local development environment

1. Clone the code:

```
$ git clone --recurse-submodules https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm
```

2. Change directory into the cloned code:

```
$ cd aegir-webhooks-dev-vm
```

3. Provision local Aegir server VM:

```
$ vagrant up aegir
```

Next, you can [run webhook tests locally](/how-to/run-local-tests).
