---
title: "Docs"

---

Aegir Webhooks Documentation
============================

Aegir Webhooks
--------------

Aegir Webhooks is intended to be used to enable Aegir Servers to be created and deleted in response to webhooks.
It also supports configuring Services on these Servers, and adding and removing them to and from Webpack clusters.
Finally, Azure scalesets can be mapped to Webpack clusters such that scaleset operations are automatically reflected in Aegir.

Documentation Index
-------------------

Use the menu on the left to navigate, or choose a page from the index below.

{{% children depth=2 %}}
