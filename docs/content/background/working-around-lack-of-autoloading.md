---
title: Working Around a Lack of Autoloading
weight: 30

---

Drupal 7, upon which Aegir 3 is built, does not natively support PHP autoloading, as we see in Drupal 8+. As such, object-oriented programming is somewhat more cumbersome.

This mostly just come down to manually loading traits and classes before they are used. For the simplest example, let's look at [BaseProcessor](../../reference/modules/hosting_webhooks/base-processor):

```php
<?php
/**
 * @file
 * Processor plugin class.
 */

namespace HostingWebhooks;

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/ProcessorTrait.php';

use \HostingWebhooks\ProcessorTrait;

/**
 * Base class for Hosting Webhooks Processors.
 */
abstract class BaseProcessor implements \Webhook_Plugins_Processor_Interface {

  use ProcessorTrait;

}
```

This line includes the proper file for the [ProcessorTrait](../../reference/modules/hosting_webhooks/processor-trait):
```
/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/ProcessorTrait.php';
```

We do this throughout the codebase, whenever a class or trait uses or extends another.
