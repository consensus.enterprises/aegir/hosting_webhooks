---
title: Background information
weight: 30

---

This section provides more in-depth discussions for broader context, and to
explain some of the architectural decisions that have defined the project.

{{% children %}}
