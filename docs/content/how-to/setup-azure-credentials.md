---
title: Set up Azure credentials
weight: 40

---

### Configuring Azure credentials on the Command Line

Please refer to the [Azure CLI documentation](https://docs.microsoft.com/en-us/cli/azure/) for details on [installation](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt) and [obtaining the relevant credentials](https://docs.microsoft.com/en-us/cli/azure/authenticate-azure-cli). Terraform also provides [alternate documentation](https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/guides/azure_cli) on authenticating using the CLI. 

### Configuring Azure credentials on the Aegir front end

Once you have [built an Aegir server](/tutorials/setup-azure-test-environment), you will need to enter your Azure credentials on the front end. 

  1. In Aegir, click on Hosting > Azure Credentials in the menu at the top of the screen.
  ![Azure credentials navigation](/images/manual-testing/integration-aegir-navigation.png)
  1. Fill in your Azure Tenant ID, Client ID and Client Secret, then click Save Configuration.
  ![Fill in Azure credentials](/images/manual-testing/integration-aegir-azure-credentials.png)

### Next steps

Once you have configured the Aegir server with your Azure credentials, you are ready to:

 * [configure Azure to call webhooks on your Aegir server](/tutorials/configure-azure-webhooks), and then
 * [manually test Azure webhooks](/tutorials/run-azure-tests).
