---
title: Run local tests
weight: 31

# TODO

---

### Dependencies

  1. Ensure you have [set up your local development environment](/tutorials/setup-local-dev-environment).
  1. Bootstrap Drumkit:
```
. d
```
  1. Install Behat:
```
make behat
```

### Run all local tests

  1. Run Behat:
```
behat
```
  This will run all of the local tests.

### Run component tests

* Run server tests:

```
behat --tags="@server&&~@slow&&~@needs-manual-config"
```

* Run webpack tests:

```
behat --tags="@webpack&&~@slow&&~@needs-manual-config"
```

### Run a specific test

See the full list of tests [here](/reference/tests).

```
behat features/<test_file_name>.feature
```

### Run end-to-end local tests (note: slow!):

```
behat --tags="@slow&&~@needs-manual-config"
```

### Other useful Behat options (for troubleshooting)

```
behat --stop-on-failure
```
