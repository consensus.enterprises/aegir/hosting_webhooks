---
title: Install Hosting Webhooks
weight: 30

---

To enable `hosting_webhooks` on your Aegir server, we recommend using [consensus.aegir](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir/), our Ansible role for installing, configuring and customizing Aegir.

[Here](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/azure/ansible/aegir-image.yml) is an example, from the [`aegir-webhooks-dev-vm project`](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm), of an Ansible playbook that can be used to configure a custom Aegir that supports `hosting_webhooks`. Take note of the variables defined in that playbook, which override the `consensus.aegir` defaults with the required configuration.

You will likely need to change at least the `aegir_frontend_url` and `aegir_admin_email` variables to suit your local needs. Take note, also, of the use of `aegir_stop_before_site_install: True`, which is used to stop the build before it is complete, as part of the creation of a Packer image for the Aegir server. If you want the build to proceed from start to finish, simply remove this line from the playbook. If you would like more information about producing a Packer image for your Aegir server, please see [the documentation on test environment setup](/tutorials/setup-azure-test-environment/).
