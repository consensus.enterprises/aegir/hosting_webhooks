<?php
/**
 * @file
 * LoggerTrait trait.
 */

namespace HostingWebhooks;

/**
 * Trait to simplify logging.
 */
trait LoggerTrait {

  /**
   * Return array of variables for interpolation into log messages.
   */
  protected function getLogVars() {
    return [
      '@plugin' => get_class($this),
    ];
  }

  /**
   * Log that a webhook has been received.
   */
  protected function logTriggered($message = '@plugin webhook plugin triggered.') {
    return $this->log($message);
  }

  /**
   * Log that a webhook has succeeded.
   */
  protected function logSuccess($message = '@plugin webhook plugin succeeded.') {
    return $this->log($message);
  }

  /**
   * Log an informational notice.
   */
  protected function logNotice($message, $vars = []) {
    return $this->log($message, $vars, WATCHDOG_NOTICE);
  }

  /**
   * Log a warning.
   */
  protected function logWarning($message, $vars = []) {
    return $this->log($message, $vars, WATCHDOG_WARNING);
  }

  /**
   * Log that a webhook has failed.
   */
  protected function logFailure($exception_message = '', $status = '500 Internal Server Error', $message = '@plugin webhook plugin failed.') {
    if (!empty($exception_message)) {
      $message .= ' Error message: ';
      $message .= $exception_message;
    }

    $this->log($message, [], WATCHDOG_ERROR);
    drupal_add_http_header('Status', $status);
    drupal_exit();
  }

  /**
   * Log a formatted message to the database log.
   */
  protected function log(string $message, array $vars = [], int $level = WATCHDOG_INFO) {
    if (empty($vars)) {
      $vars = $this->getLogVars();
    }
    return watchdog('hosting_webhooks', $message, $vars, $level);
  }

  /**
   * Log the entire payload to the database log.
   */
  protected function logPayload() {
    $payload = $this->getPayload();
    $message = "@plugin webhook plugin received the following payload:\n";
    $message .= '<pre>' . json_encode($payload, JSON_PRETTY_PRINT) . '</pre>';
    return $this->logNotice($message);
  }

}
