<?php
/**
 * @file
 * Processor plugin class.
 */

namespace HostingWebhooks;

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/ProcessorTrait.php';

use \HostingWebhooks\ProcessorTrait;

/**
 * Base class for Hosting Webhooks Processors.
 */
abstract class BaseProcessor implements \Webhook_Plugins_Processor_Interface {

  use ProcessorTrait;

}
