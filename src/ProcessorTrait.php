<?php
/**
 * @file
 * ProcessorTrait trait.
 */

namespace HostingWebhooks;

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/LoggerTrait.php';

use \HostingWebhooks\LoggerTrait;

/**
 * Trait to simplify processing webhooks.
 */
trait ProcessorTrait {

  use LoggerTrait;

  /* The unserialized payload passed to the process() method. */
  protected $payload;

  /**
   * {@inheritdoc}
   */
  public function config_form() {
    // Non-functional, but required by the interface.
  }

  /**
   * {@inheritdoc}
   */
  public function process(\stdClass $data) {
    $this->payload = $data;

    $this->logTriggered();
    $this->logPayload();

    $this->doProcessActions();

    return $this->logSuccess();
  }

  /**
   * Perform the actions for this plugin.
   */
   protected function doProcessActions() {
     // This method should be implemented/overridden in plugin classes.
   }

  /**
   * Return the webhook payload.
   */
  protected function getPayload() {
    return $this->payload;
  }

}
