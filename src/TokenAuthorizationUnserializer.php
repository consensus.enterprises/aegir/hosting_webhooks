<?php
/**
 * @file
 * Webhook unserializer plugin class.
 */

/**
 * Webhook unserializer plugin for authorizing Aegir Hosting webhooks.
 */
class TokenAuthorizationUnserializer implements Webhook_Plugins_Unserializer_Interface {

  protected const DEFAULT_AUTHORIZATION_TOKEN = '90437cca-aa3b-4b07-8ce3-db95826de868';

  /**
   * {@inheritdoc}
   */
  public function unserialize($data) {
    if (!$this->validateAuthorizationToken()) {
      drupal_add_http_header('Status', '401 Unauthorized');
      drupal_exit();
    }

    $plugin = webhook_load_unserializer('json');
    return $plugin->unserialize($data);
  }

  /**
   * Customize the webhook config form for this plugin.
   */
  public function formAlter(&$form, &$form_state) {
    // Set weights for webhook form elements, so that our config fieldset can
    // be placed properly.
    $wh_form_elements = ['info', 'title', 'description', 'unserializer', 'processor', 'enabled'];
    $weight = 0;
    foreach ($wh_form_elements as $element) {
      $form[$element]['#weight'] = $weight;
      $weight++;
    }

    $form['token_authorization'] = [
      '#type' => 'fieldset',
      '#title' => t('Token Authorization Configuration'),
      '#weight' => $form['unserializer']['#weight'] + 0.5,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['token_authorization']['token'] = [
      '#type' => 'textfield',
      '#title' => t('Authorization token'),
      '#value' => $this->getAuthorizationTokenForForm($form, $form_state),
      '#disabled' => TRUE,
    ];

    $form['token_authorization']['generate_token'] = [
      '#type' => 'submit',
      '#value' => t('Generate new token'),
      '#submit' => [ __CLASS__ . '::generateToken'],
    ];

    $form['#validate'][] = __CLASS__ . '::formValidate';
    $form['#submit'][] = __CLASS__ . '::formSubmit';
  }

  /**
   * Form validation handler to ensure a unique authorization token.
   */
  static public function formValidate($form, &$form_state) {
    // Token generation happens in a submit handler (generateToken()), so we
    // need to give it a chance to work.
    if ($form_state['input']['op'] === 'Generate new token') return;

    // Don't let users continue to use the default token.
    if ($form_state['values']['token'] === self::DEFAULT_AUTHORIZATION_TOKEN) {
      return form_set_error('token_authorization', 'Using the default authorization token is insecure. Please generate a new token.');
    }
  }

  /**
   * Form submit handler to record our token.
   */
  static public function formSubmit($form, &$form_state) {
    $webhook = $form_state['values']['machine_name'];
    $token = $form_state['values']['token'];
    return self::saveAuthorizationToken($webhook, $token);
  }

  /**
   * Add or update an authorization token for a given webhook.
   */
  static public function saveAuthorizationToken($webhook, $token) {
    if (!self::webhookHasAuthorizationToken($webhook, $token)) {
      $query = db_insert('hosting_webhooks_tokens')
        ->fields([
          'webhook' => $webhook,
          'token' => $token,
        ]);
    }
    else {
      $query = db_update('hosting_webhooks_tokens')
        ->fields([
          'token' => $token,
        ])
        ->condition('webhook', $webhook, '=');
    }
    return $query->execute();
  }

  /**
   * Determine whether a given webhook already has a token.
   */
  static public function webhookHasAuthorizationToken($webhook, $token) {
    // If we get back the default token, then this token hasn't been saved yet.
    // So it must be new.
    return self::loadAuthorizationToken($webhook) !== self::DEFAULT_AUTHORIZATION_TOKEN;
  }

  /**
   * Generate a new token.
   */
  static public function generateToken(&$form, &$form_state) {
    $form_state['rebuild'] = TRUE;
    // Store the new value, so that the form rebuild will find and use it.
    $form_state['storage']['values']['token'] = uuid_generate();
  }

  /**
   * Validate the authorization token.
   */
  protected function validateAuthorizationToken() {
    $vars = [];
    $vars['@webhook'] = $webhook = arg(1);
    $query = drupal_get_query_parameters();

    if (!array_key_exists('authorization_token', $query)) {
      watchdog('hosting_webhooks', 'Expected authorization token missing from query parameters for @webhook webhook.', $vars, WATCHDOG_WARNING);
      return FALSE;
    }

    $token = $vars['@token'] = $query['authorization_token'];

    if ($token !== $this->loadAuthorizationToken($webhook)) {
      watchdog('hosting_webhooks', 'Authorization token (@token) invalid for @webhook webhook.', $vars, WATCHDOG_WARNING);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Return the webhook's authorization token.
   */
  protected function getAuthorizationTokenForForm($form, $form_state) {
    // Allow the "Generate new token" button to override the saved value.
    if (isset($form_state['storage']['values']['token'])) return $form_state['storage']['values']['token'];

    $webhook = isset($form['info']['machine_name']['#value']) ? $form['info']['machine_name']['#value'] : $form['info']['machine_name']['#default_value'];
    return $this->loadAuthorizationToken($webhook);
  }

  /**
   * Return the webhook's authorization token.
   */
  static public function loadAuthorizationToken($webhook) {
    $query = db_select('hosting_webhooks_tokens', 'hwt')
      ->fields('hwt', ['token'])
      ->condition('hwt.webhook', $webhook);
    $token = $query->execute()->fetchField();

    // If we don't have a saved token, use the default.
    if (empty($token)) return self::DEFAULT_AUTHORIZATION_TOKEN;

    return $token;
  }

}
