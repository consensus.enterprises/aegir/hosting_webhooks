<?php
/**
 * @file
 * The hosting feature definition for hosting_webhooks_example.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_webhooks_example_hosting_feature() {
  $features['webhooks_example'] = [
    'title' => t('Example Webhook'),
    'description' => t('Provides an example webhook that logs to the database.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_webhooks_example',
    'group' => 'experimental',
  ];

  return $features;
}

