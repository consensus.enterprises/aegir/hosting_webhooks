<?php
/**
 * @file
 * Webhook processor plugin class.
 */

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/BaseProcessor.php';

use \HostingWebhooks\BaseProcessor;

/**
 * Logs webhook data to the database.
 */
class LogDataProcessor extends BaseProcessor {

  /**
   * {@inheritdoc}
   */
  public function doProcessAction() {
    // Nothing to do here.
  }

  /**
   * {@inheritdoc}
   */
  protected function getLogVars() {
    return parent::getLogVars() + [
      '@webhook' => $this->getPayload()->webhook,
      '@message' => $this->getPayload()->info->message,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function logSuccess($message = 'Webhook received (@webhook). Message: @message') {
    return parent::logSuccess($message);
  }

}
