<?php
/**
 * @file
 * hosting_webhooks_example.webhook.inc
 */

/**
 * Implements hook_webhook_default_config().
 */
function hosting_webhooks_example_webhook_default_config() {
  $export = array();

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '1';
  $webhook->title = 'Hosting Webhooks Example: Log Data';
  $webhook->machine_name = 'example_logdata';
  $webhook->description = 'Webhook for logging data to the database.';
  $webhook->unserializer = 'json';
  $webhook->processor = 'log_data';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['example_logdata'] = $webhook;

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '1';
  $webhook->title = 'Hosting Webhooks Example: Token Authorization';
  $webhook->machine_name = 'example_tokenauth';
  $webhook->description = 'Webhook for testing token authorization.';
  $webhook->unserializer = 'token_authorization';
  $webhook->processor = 'log_data';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['example_tokenauth'] = $webhook;

  return $export;
}
